package testcucumber;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(strict = false, features = "src/test/resources/testcucumber/", format = {"pretty", "json:target/cucumber.json"})
public class RunCucumberTest {
}
