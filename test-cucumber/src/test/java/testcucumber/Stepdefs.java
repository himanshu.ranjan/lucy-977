package testcucumber;

import java.util.*;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;
import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;


public class Stepdefs {
    private String url;
    private int actualAnswer;

    @Given("^url is \"([^\"]*)\"$")
    public void set_url(String passed_url) {
        this.url = passed_url;
	System.out.println("Here is the url" + this.url);
    }

    @When("^I hit Get with user \"([^\"]*)\"$")
    public void get_user(String username) throws IOException {
	URL obj = new URL(this.url + username);
	HttpURLConnection con = (HttpURLConnection) obj.openConnection();
	con.setRequestMethod("GET");
	con.setRequestProperty("User-Agent", "Mozilla/5.0");
	int responseCode = con.getResponseCode();
	System.out.println("GET Response Code :: " + responseCode);
	this.actualAnswer = responseCode;
    }

    @Then("^I should get status (\\d+)$")
    public void match_status(int expectedAnswer) {
        assertEquals(expectedAnswer, actualAnswer);
    }

}
